import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import App from './App.vue'
import UploadFiles from './components/UploadFiles.vue'

Vue.config.productionTip = false

const vuetifyOptions = { }
Vue.use(Vuetify)
Vue.component('upload-files', UploadFiles)

new Vue({
  render: h => h(App),
  vuetify: new Vuetify(vuetifyOptions)
}).$mount('#app')